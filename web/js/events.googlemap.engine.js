/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function LayerThemes(themesMap) {

    var myCache = themesMap;
    if (typeof themesMap === 'undefined') {
        myCache = {};

        myCache['DEFAULT'] = {
            key: LayerThemes.DEFAULT_CODE,
            //icon: 'RedMarker.png',
            color: LayerThemes.DEFAULT_COLOR 
        };
        
    };


    this.themesCache = myCache;
};

LayerThemes.DEFAULT_CODE = 'DEFAULT';
LayerThemes.DEFAULT_COLOR = '#FF0000';
LayerThemes.prototype.getLayerTheme = function (key) {
    var providedKey = key;
    if (!providedKey) {
        providedKey = LayerThemes.DEFAULT_CODE;
    }
    return this.themesCache[providedKey];
};

function Layer(type) {
    this.themeCode = LayerThemes.DEFAULT_CODE;
    this.type = type;
    this.components = {};
    this.newComponentCounter = 0;
};
Layer.TYPE_POINT = "0";
Layer.MAP = '';
Layer.SIMPLIFIED_ELEMENT_TYPE_MARKER = "0";
Layer.generateSimplifiedElementType = function (googleMapOverlayType) {
    switch (googleMapOverlayType) {
        case google.maps.drawing.OverlayType.MARKER:
            return Layer.SIMPLIFIED_ELEMENT_TYPE_MARKER;
            break;
        default:
            return Layer.SIMPLIFIED_ELEMENT_TYPE_IMAGE;
            break;
    }
};
Layer.prototype = {
    getAllComponents: function () {
        return this.components;
    },
    getTheme: function () {
        return this.themeCode;
    }, 
    getType: function(){          
        return this.type;
    },
    addNewComponent: function(eventsGoogleMapObject, googleMapOverlayType){
        var keyCode = this.type + "-" + this.newComponentCounter;
        eventsGoogleMapObject.setOptions(
                {
                    description: '',
                    title: '',
                    keyCode: keyCode, 
                    type: Layer.generateSimplifiedElementType(googleMapOverlayType)
                }
        );
        this.components[keyCode] = eventsGoogleMapObject;
        this.newComponentCounter++;
    },
    removeComponent: function(eventsGoogleMapObject){
        if (eventsGoogleMapObject) {
            var keyCode = eventsGoogleMapObject.keyCode;
            var components = this.components;

            if (components.hasOwnProperty(keyCode))
                delete components[keyCode];
        }
    }
};

function PointLayer(themeCode) {
    Layer.call(this, Layer.TYPE_POINT);
    if (themeCode)
        this.themeCode = themeCode;
}
PointLayer.prototype = Object.create(Layer.prototype);
PointLayer.prototype.addNewPoint = function (key, description, latLng) {
    description = description.split('|')
    this.components[key] = new google.maps.Marker({
        position: latLng,
        title: description[0],
        description: description[1],
        keyCode: key,
        type: Layer.SIMPLIFIED_ELEMENT_TYPE_MARKER
    });
};
PointLayer.prototype.getAllPersistableComponentsInJSONForm = function(){
    return JSON.stringify(this.getAllPersistableComponents());
};
PointLayer.prototype.getAllPersistableComponents = function(){
    var rawComponents = this.getAllComponents();
    var toBeJSONified = {};
    
    for(var key in rawComponents){
        if(rawComponents.hasOwnProperty(key)){
            
            var point = rawComponents[key];
            
                var javascriptInterimObject = {
                    description: point.description, 
                type: point.type
                };

            if(Layer.SIMPLIFIED_ELEMENT_TYPE_MARKER === point.type){
                var positionLatLng = point.getPosition();
                var pointArray = [];
                pointArray.push({plotSequence: key, x: positionLatLng.lat(), y: positionLatLng.lng()});
  
                javascriptInterimObject['pointArray'] = pointArray;                                      
                    
                toBeJSONified[key] = javascriptInterimObject;    
            }          
        }
    };
    
    return toBeJSONified;
};
PointLayer.generateInstance = function (_pointMap, themeCode) {
    var generatedPointLayer = new PointLayer(themeCode);
    if(_pointMap!=null){
        $.each(_pointMap, function(key, pointMap){
            for (var key in pointMap) {
                if (pointMap.hasOwnProperty(key)) {
                    var rawPointData = pointMap[key];
                    var coordinates = rawPointData[2].split(',');
                    var googleMapLatLng = GoogleMap.generateGoogleMapCoordinate(coordinates[0],coordinates[1]);
                    generatedPointLayer.addNewPoint(key, rawPointData[0], googleMapLatLng);
                }
            }
        })
    }

    return generatedPointLayer;
    
};

function EventsTransactionStatus(handledMap) {
    this.handledMap = handledMap;
    this.layersEditMode = false;
    this.layerEditedIsPointType = false;
}

EventsTransactionStatus.prototype = {
    isInLayersEditMode: function () {
        return this.layersEditMode;
    },
    isLayerEditedPointType: function () {
        return this.layerEditedIsPointType;
    },

    setToLayersViewMode: function (layer,color) {
        this.layersEditMode = false;
        this.layerEditedIsPointType = false;

    },
    setToLayersAddMode: function(){
        this.layersEditMode = true;
        this.layerEditedIsPointType = false;
    }
};

function GoogleMapDrawingManager(handleMap) {
    this.handledMap = handleMap;

    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER
        },
    });

    drawingManager.setMap(handleMap.map);
    
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
        var newComponent = event.overlay;
        
        handleMap.currentManagedLayer.addNewComponent(newComponent, event.type);
        newComponent.attachBehaviorManager(handleMap.objectBehaviorManager);
        handleMap.eventDispatcher.raiseEvent(
                    GoogleMapDrawingManager.EVENTS_DRAWING_COMPLETE,[newComponent]);
    });
    
    this.instance = drawingManager;
}
GoogleMapDrawingManager.EVENTS_DRAWING_REMOVED = 'drawingRemoved';
GoogleMapDrawingManager.EVENTS_DRAWING_COMPLETE = 'drawingComplete';
GoogleMapDrawingManager.EVENTS_DRAWING_CHANGED = 'drawingChange';
GoogleMapDrawingManager.IMAGE_FRAME_COMPLETE = 'imageFrameComplete';
GoogleMapDrawingManager.prototype = {
    disable: function(){
        this.instance.setMap(null);
    },
    enable: function(){
        this.instance.setMap(this.handledMap.map);
    },
   
};

function EVENTSGoogleMapObjectToolTip(selector){
    this.selector = selector;
    this._id = EVENTSGoogleMapObjectToolTip.generateID();
//    var htmlBox = document.getElementById('myinfo');
//    
//    htmlBox.addEventListener("mouseover",function(){
//        var selectionFired = new CustomEvent("mouseoverinfodiv",{});
//        document.dispatchEvent(selectionFired);
//    });
}
EVENTSGoogleMapObjectToolTip.generateID = (function () {
    var counter = 0;
    return function () {return counter += 1;};
})();
EVENTSGoogleMapObjectToolTip.prototype = {
    show: function(description, x, y){
//        var htmlBox = document.getElementById(this.selector);
//        var p = htmlBox.getElementsByTagName('p')[0];
//        if((typeof p !== "undefined")) p.innerHTML = description;
//        htmlBox.style.left=x+'px';
//        htmlBox.style.top=y+'px';
//        htmlBox.style.opacity=0;

        EVENTSJavaScriptUtil.fadeIn(this.selector,0);
    }, 
    hide: function(){
//        var htmlBox = document.getElementById(this.selector);
//        if(0 < htmlBox.style.opacity){
//            EVENTSJavaScriptUtil.fadeOut(this.selector, parseInt(htmlBox.style.opacity))
//        }
    }
};

function EVENTSGoogleMapObjectBehaviorHandler(toBeHandledMap, toolTip){
    this.handledMap = toBeHandledMap;
    this.toolTip = toolTip;
    this.currentInfoWindow;
    this.currentlyEditingInfoWindow = false;
    this._currentlyOnDiv = false;
    this._toolTippedObject;

    document.addEventListener("mouseoverinfodiv",function(){
        this._currentlyOnDiv = true;
    });
    
    google.maps.event.addListener(toBeHandledMap.map, 'dragstart', function(){  
        toolTip.hide();
        this._toolTippedObject = null;
    });
}

EVENTSGoogleMapObjectBehaviorHandler.generateInstance = function(handledMap, toolTipSelector){
    var toolTip = new EVENTSGoogleMapObjectToolTip(toolTipSelector);
    var generatedObject = new EVENTSGoogleMapObjectBehaviorHandler(handledMap, toolTip);
    
    
    return generatedObject;
};

EVENTSGoogleMapObjectBehaviorHandler.TOOL_TIP_DELAY = 400;
EVENTSGoogleMapObjectBehaviorHandler.EVENTS_DESCRIPTION_CHANGED = "descriptionChanged";
EVENTSGoogleMapObjectBehaviorHandler.EVENTS_DBL_CLICKED = "dblclick";
EVENTSGoogleMapObjectBehaviorHandler.EVENTS_DRAWING_DRAGEND = "dragend";
EVENTSGoogleMapObjectBehaviorHandler.prototype = {
    manageBehavior: function (mapObject) {
        
        switch(mapObject.type){
            case Layer.SIMPLIFIED_ELEMENT_TYPE_IMAGE:
                executeMapOverrideImageBehavior(mapObject);
                break;
            case Layer.SIMPLIFIED_ELEMENT_TYPE_MARKER:
                this.attachDoubleClickBehaviour(mapObject); 
                break;
            case Layer.SIMPLIFIED_ELEMENT_TYPE_POLYGON:
            case Layer.SIMPLIFIED_ELEMENT_TYPE_CIRCLE:
            case Layer.SIMPLIFIED_ELEMENT_TYPE_RECTANGLE:
                this.attachToolTip(mapObject);
                this.attachDoubleClickBehaviour(mapObject); 
                break;
        }
        this.attachEventHandler(mapObject);
        this.trackObjectWhenSelected(mapObject);
    },
    
    trackObjectWhenSelected: function (mapObject) {
        var map = this.handledMap;
        google.maps.event.addListener(mapObject, 'click', function () {
            map._currentSelectedElement = mapObject;
        });
    },
    
    attachEventHandler: function(mapObject){
        var map = this.handledMap;
        switch(mapObject.type){
            case Layer.SIMPLIFIED_ELEMENT_TYPE_RECTANGLE:
                setEventHandler(mapObject, mapObject,'bounds_changed');
                break;
            case Layer.SIMPLIFIED_ELEMENT_TYPE_POLYGON:
                mapObject.getPaths().forEach(function (path, index) {
                    setEventHandler(path, mapObject,'insert_at');
                    //setEventHandler(path, mapObject,'remove_at');
                    setEventHandler(path, mapObject,'set_at');
                });
                break;
            case Layer.SIMPLIFIED_ELEMENT_TYPE_CIRCLE:  
                //setEventHandler(mapObject, mapObject,'center_changed');
                setEventHandler(mapObject, mapObject,'radius_changed');
                break;
            case Layer.SIMPLIFIED_ELEMENT_TYPE_MARKER:
                setEventHandler(mapObject, mapObject,'dragend');
                break;
            case Layer.SIMPLIFIED_ELEMENT_TYPE_IMAGE:
                break;
        } 
        
        function setEventHandler(handled, raisedObject, googleMapEvent) {
            google.maps.event.addListener(handled, googleMapEvent, function () {
                map.eventDispatcher.raiseEvent(
                        GoogleMapDrawingManager.EVENTS_DRAWING_CHANGED, [raisedObject]);
            });
        }
    },
    
    executeMapOverrideImageBehavior: function(mapObject){
        var me = this;
        var handledMap = me.handledMap;     
              
        //(4)Create a div element to display the HTML strings.
//        var htmlBox = document.createElement("div");
//        htmlBox.innerHTML = mapObject.description;
//        htmlBox.style.width = "150px";
//        htmlBox.style.height = "70px";
//        htmlBox.style.color = "black";

//        //(5)Create a textarea for edit the HTML strings.
//        var textBox = document.createElement("textarea");
//        textBox.innerText = mapObject.description;
//        textBox.style.width = "150px";
//        textBox.style.height = "70px";
//        textBox.style.display = "none";
//        textBox.style.color = "black";
//        textBox.placeholder = "Add title here";

        //(6)Create a div element for container.
//        var container = document.createElement("div");
//        container.style.position = "relative";
//        container.appendChild(htmlBox);
//        container.appendChild(textBox);

        //(7)Create a button to switch the edit mode
        var editBtn = document.createElement("button");
        editBtn.innerText = "Edit";
        container.appendChild(editBtn);
        
        me.toolTip.hide();
        me.currentlyEditingInfoWindow = true;

        if (me.currentInfoWindow) {
            me.currentInfoWindow.close();
        }

        var infoWnd = new google.maps.InfoWindow({
            content: container
        });

        var centerPosition = mapObject.getCenter();

        infoWnd.setPosition(centerPosition);
        infoWnd.open(handledMap.map);

        handledMap.map.panTo(centerPosition);
        handledMap.lockMap();

        me.currentInfoWindow = infoWnd;
    },
    
    attachToolTip: function (googleMapObject) {
        var prevTime;
        var me = this;
        google.maps.event.addListener(googleMapObject, 'click', function () {
            me.toolTip.hide();
            me._toolTippedObject = null;
        });
        google.maps.event.addListener(googleMapObject, 'drag', function () {
            me.toolTip.hide();
            me._toolTippedObject = null;
        });
        google.maps.event.addListener(googleMapObject, 'dragend', function () {
            me.handledMap.eventDispatcher.raiseEvent(
                EVENTSGoogleMapObjectBehaviorHandler.EVENTS_DRAWING_DRAGEND, [googleMapObject]);
        });
        google.maps.event.addListener(googleMapObject, 'mouseover', function () {
            this.setOptions({fillOpacity: PolygonLayer.FILL_OPACITY_HIGHLIGHTED});

            if (!EVENTSJavaScriptUtil.notFast(prevTime)) {
                me.toolTip.hide();
                me._toolTippedObject = null;
                me._currentlyOnDiv = false;
            }
            if (!me.currentlyEditingInfoWindow && (me._toolTippedObject !== googleMapObject.keyCode) && EVENTSJavaScriptUtil.notFast(prevTime)) {

                setTimeout(
                        function () {
                            if (!me._currentlyOnDiv) {
                                var latLng = googleMapObject.getCenter();
                                var equivalentPoint = me.handledMap.convertLatLngToPoint(latLng);

                                me.toolTip.show(
                                        googleMapObject.description, equivalentPoint.x, equivalentPoint.y);
                            } else {
                                me._currentlyOnDiv = false;
                            }
                        },
                        EVENTSGoogleMapObjectBehaviorHandler.TOOL_TIP_DELAY * 2
                        );
            }

            prevTime = new Date().getTime();
        });
        google.maps.event.addListener(googleMapObject, 'mouseout', function () {
            this.setOptions({fillOpacity: PolygonLayer.FILL_OPACITY});
            me._toolTippedObject = googleMapObject.keyCode;
        });
    },
    
    attachDoubleClickBehaviour: function(googleMapObject) {
        var me = this;
        var handledMap = me.handledMap;

        //(3)Set a flag property which stands for the editing mode.
        googleMapObject.set("editing", false);

        //(4)Create a div element to display the HTML strings.
        var htmlBox = document.createElement("div");
        htmlBox.innerHTML = googleMapObject.description;
        htmlBox.style.width = "150px";
        htmlBox.style.height = "70px";
        htmlBox.style.color = "black";
        
        //(5.1)Create a textarea for edit the HTML strings.
        var span = document.createElement("span");
        span.innerHTML = '<b id="element_information">Element Information</b>';
        span.style.color = "green";
        
        //(5.1)Create a textarea for edit the HTML strings.
        var textBox = document.createElement("textarea");
        textBox.innerHTML = googleMapObject.title;
        textBox.style.display = "none";
        textBox.style.color = "black";
        textBox.placeholder = "Add title here";
        

        //(5.2)Create a textarea for edit the HTML strings.
        var textBox2 = document.createElement("textarea");
        textBox2.innerHTML = googleMapObject.description;
        textBox2.style.color = "black";
        textBox2.placeholder = "Add description here";        

        //(6)Create a div element for container.
        var container = document.createElement("div");
        container.style.position = "relative";
        container.appendChild(htmlBox);
        container.appendChild(span);
        container.appendChild(textBox);
        container.appendChild(textBox2);

        //(7)Create a button to switch the edit mode
        var saveBtn = document.createElement("button");
        saveBtn.innerHTML = "Save";
        saveBtn.style.float = "right";
        container.appendChild(saveBtn);
        
        //(7)Create a button to switch the edit mode
        var editBtn = document.createElement("button");
        editBtn.innerHTML = "Edit";
        //container.appendChild(editBtn);

        //(9)The info window appear when the marker is clicked.
        var dbClickOrigin;
        google.maps.event.clearListeners(googleMapObject, "dblclick");
        google.maps.event.addListener(googleMapObject, "dblclick", function () {
            var transactionHandler = me.handledMap.getTransactionHandler();
            //ariel
            if (!me.currentlyEditingInfoWindow && transactionHandler.isInLayersEditMode()) {
            //if (!me.currentlyEditingInfoWindow) {
                me.toolTip.hide();
                me.currentlyEditingInfoWindow = true;
                googleMapObject.disableEditing(true);

                if (me.currentInfoWindow) {
                    me.currentInfoWindow.close();
                }

                var infoWnd = new google.maps.InfoWindow({
                    content: container
                });

                //googleMapObject.set("editing", true);

                dbClickOrigin = googleMapObject.getCenter();
                var currentZoomLevel = handledMap.map.getZoom();

                infoWnd.setPosition(dbClickOrigin);
                infoWnd.open(handledMap.map);

                handledMap.map.setZoom(currentZoomLevel);
                handledMap.map.setCenter(dbClickOrigin);

                me.currentInfoWindow = infoWnd;

                google.maps.event.addDomListener(infoWnd, "closeclick", function () {
                    disableMapLock();
                    googleMapObject.disableEditing(false);
                    me.currentlyEditingInfoWindow = false;
                    me._toolTippedObject = null;
                });

                googleMapObject.set("editing", true);

            }else{
                handledMap.eventDispatcher.raiseEvent(
                        EVENTSGoogleMapObjectBehaviorHandler.EVENTS_DBL_CLICKED, [googleMapObject]);
            }
        });

        //(10)Switch the mode. Since Boolean type for editing property,
        //the value can be change just negation.
        google.maps.event.addDomListener(editBtn, "click", function () {
            googleMapObject.set("editing", !googleMapObject.editing);
        });
        
        google.maps.event.addDomListener(saveBtn, "click", function () {
            handledMap.eventDispatcher.raiseEvent(
                        EVENTSGoogleMapObjectBehaviorHandler.EVENTS_DESCRIPTION_CHANGED, [googleMapObject]);
        });

        //(11)A (property)_changed event occur when the property is changed.
        google.maps.event.addListener(googleMapObject, "editing_changed", function () {
            if (googleMapObject.editing) {
                handledMap.map.panTo(dbClickOrigin);
                handledMap.lockMap();
                textBox.style.display = "block";
                htmlBox.style.display = "none";
                editBtn.innerText = "Done";
            } else {
                disableMapLock();
                editBtn.innerText = "Edit";
                handledMap.eventDispatcher.raiseEvent(
                        EVENTSGoogleMapObjectBehaviorHandler.EVENTS_DESCRIPTION_CHANGED, [googleMapObject]);
            }
        });

        //(12)A change DOM event occur when the textarea is changed, then set the value into htmlBox.
        google.maps.event.addDomListener(textBox, "change", function () {
            htmlBox.innerHTML = textBox.value;
            googleMapObject.set("html", textBox.value);
            googleMapObject.description = textBox.value;
            if (googleMapObject.type === Layer.SIMPLIFIED_ELEMENT_TYPE_MARKER) {
            googleMapObject.title = textBox.value;
            }
        });

        //(12)A change DOM event occur when the textarea is changed, then set the value into htmlBox.
        google.maps.event.addDomListener(textBox2, "change", function () {
            htmlBox.innerHTML = textBox2.value;
            googleMapObject.set("html", textBox2.value);
            googleMapObject.description = textBox2.value;
        });

        function disableMapLock() {
            handledMap.unlockMap(googleMapObject.keyCode);
            textBox.style.display = "none";
            htmlBox.style.display = "block";
        }
    },
    
    removeToolTip: function(){
        var me = this;
        me.toolTip.hide();
        me._toolTippedObject = null;
        me._currentlyOnDiv = false;
    }
};

function EVENTSImageObjectHandler(imageFrame){
    this.imageFrame = imageFrame;
}

EVENTSImageObjectHandler.prototype = {
    
}

function GoogleMap(mapId, mapOptions, layersTheme) {
    this.map = new google.maps.Map(document.getElementById(mapId), mapOptions);
    this.transactionHandler = new EventsTransactionStatus(this);
    this.eventDispatcher = new EVENTSEventDispatcher();
    var applicableThemeCache = layersTheme;

    if (typeof layersTheme === 'undefined') {
        applicableThemeCache = new LayerThemes();
    }

    this.themeCache = applicableThemeCache;
    this.drawingManager = new GoogleMapDrawingManager(this);
    this.objectBehaviorManager = EVENTSGoogleMapObjectBehaviorHandler.generateInstance(this,'myinfo');
    this.currentManagedLayer;
    this._currentSelectedElement;
}

GoogleMap.prototype = {
    getEventDispatcher: function(){
        return this.eventDispatcher;
    },
    lockMap: function(){
        this.map.setOptions({
            draggable: false,
            scrollwheel: false,
            panControl: false
        });
        this.drawingManager.disable();
        this.disableEditingOfMapObjects(true);
    },
    unlockMap: function(excemptedKey){
        this.map.setOptions({
            draggable: true,
            scrollwheel: true,
            panControl: true
        });
        this.drawingManager.enable();
        this.disableEditingOfMapObjects(false,excemptedKey);
    },
    getMap: function(){
        return this.map;
    },
    getCurrentManagedLayer: function(){
        return this.currentManagedLayer;
    },
    getThemeCache: function () {
        return this.themeCache;
    },
    getTransactionHandler: function () {
        return this.transactionHandler;
    },
    plotPointLayer: function (pointLayer, onEditMode) {
        //this.removeAllCurrentComponents();
        var me = this;
        var pointsMap = pointLayer.getAllComponents();
        console.log(pointsMap); 
        for (var key in pointsMap) {
            if (pointsMap.hasOwnProperty(key)) {
                var point = pointsMap[key];
                point.disableEditing(!onEditMode);
                point.setOptions({
                    map: me.map,
                    //icon: 'RedMarker.png',
                    draggable: true
                });
               
                point.attachBehaviorManager(me.objectBehaviorManager);
            }
        }

        this.currentManagedLayer = pointLayer;
    },
    plotPolygonLayer: function (polygonLayer,color,onEditMode) {
        //if(onEditMode) this.removeAllCurrentComponents();
        if(polygonLayer){
            var me = this;
            var polygonMap = polygonLayer.getAllComponents();
            //var applicableTheme = me.themeCache.getLayerTheme(polygonLayer.getTheme());
            for (var key in polygonMap) {
                if (polygonMap.hasOwnProperty(key)) {
                    var polygon = polygonMap[key];
                    polygon.disableEditing(!onEditMode);
                    polygon.setOptions({
                        map: me.map,
                        strokeColor: color,
                        fillColor: color
                    });
                    polygon.attachBehaviorManager(me.objectBehaviorManager);
                }
            }
            this.currentManagedLayer = polygonLayer;    
        }       
    },
    plotImageLayer: function (imageLayer,color,onEditMode) {
        //if(onEditMode) this.removeAllCurrentComponents();
        if(imageLayer){
            
        }       
    },
    disableEditingOfMapObjects: function(isTrue, keyCode){
        var managedLayer = this.currentManagedLayer;
        
        var componentsMap = managedLayer.getAllComponents();

        for (var key in componentsMap) {
            if (componentsMap.hasOwnProperty(key)&&(key!==keyCode)) {
                var component = componentsMap[key];
                component.disableEditing(isTrue);
            }
        }
    },
    convertLatLngToPoint: function(latLng){
        var map = this.map;
        var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
	var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
	var scale = Math.pow(2, map.getZoom());
	var worldPoint = map.getProjection().fromLatLngToPoint(latLng);
	return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
    },
    removeAllCurrentComponents: function(){
        var managedLayer = this.currentManagedLayer;
        if (managedLayer) {
            var componentsMap = managedLayer.getAllComponents();

            for (var key in componentsMap) {
                if (componentsMap.hasOwnProperty(key)) {
                    var component = componentsMap[key];
                    component.setMap(null);
                }
            }
        }
    },
    removeComponentsByLayer: function(managedLayer){
        var componentsMap = managedLayer;
        for (var key in componentsMap) {
            if (componentsMap.hasOwnProperty(key)) {
                var component = componentsMap[key];
                component.setMap(null);
            }
        }
    }
};

GoogleMap.generateGoogleMapCoordinate = function (lat, lang) {
    return new google.maps.LatLng(lat, lang);
};

function EventsMapFactory(zoomLevel, centerLatLng) {
    this.mapOptions = {
        zoom: zoomLevel,
        center: centerLatLng,
        disableDoubleClickZoom: true
    };
    
    if(!google.maps.Marker.prototype.getCenter){
        google.maps.Marker.prototype.getCenter = function () {
            return this.getPosition();
        };    
    }
    
    function attachBehaviorManager(googleMapObject, behaviorManager){
        if(behaviorManager && (behaviorManager instanceof EVENTSGoogleMapObjectBehaviorHandler)){
            behaviorManager.manageBehavior(googleMapObject);
        }
    }
}

EventsMapFactory.prototype = {
    generateMap: function (mapId, layersTheme) {
        var me = this;
        var generatedMap = new GoogleMap(mapId, me.mapOptions, layersTheme);
        Layer.MAP = generatedMap;
        return generatedMap;
    }
};

function EVENTSJavaScriptUtil(){}
/**
 * Custom fadeOut to make engine independent of jQuery
 * @param {type} id
 * @param {type} val
 */
EVENTSJavaScriptUtil.fadeOut = function(id, val) {
    if (isNaN(val)) {
        val = 9;
    }
    if(val < 0)
        val = 0;
    document.getElementById(id).style.opacity =  '0.' + val;
    //For IE
    document.getElementById(id).style.filter = 'alpha(opacity=' + val + '0)';
    if (val > 0) {
        val = val - 2;
        setTimeout('EVENTSJavaScriptUtil.fadeOut("' + id + '",' + val + ')', 90);
    } else {
        return true;
    }
};

/**
 * Custom fadeIn to make engine independent of jQuery
 * @param {type} id
 * @param {type} val
 */
EVENTSJavaScriptUtil.fadeIn = function(id, val) {
    if (isNaN(val)) {
        val = 0;
    }
    var selectedElement = document.getElementById(id);
    if(val > 9){
        selectedElement.style.opacity =  '1.0';
        //For IE
        selectedElement.style.filter = 'alpha(opacity=100)';
        val = 9;
    } else{
        selectedElement.style.opacity =  '0.' + val;
        //For IE
        selectedElement.style.filter = 'alpha(opacity=' + val + '0)';
    }

    if (val < 9) {
        val = val + 2;
        setTimeout('EVENTSJavaScriptUtil.fadeIn("' + id + '",' + val + ')', 90);
    } else {
        return;
    }
};

/**
 * Used by Google Map Objects handler to determine if user interaction is fast or not.
 * Fast means that the time elapsed after this function is called is less than a second.
 * 
 * @param {type} prevTime - time to compare to current time this function is called
 * @returns {Boolean} - if time compared is greater than 1 second
 */
EVENTSJavaScriptUtil.notFast = function (prevTime) {
    if (!prevTime)
        return true;
    var timeDiffRaw = new Date().getTime() - prevTime;
    return (2000 < timeDiffRaw);
};


var EVENTSEventDispatcher = function () {
    this._events = {};
    this._isEnabled = true;
};
EVENTSEventDispatcher.prototype = {
    addListener: function (eventName, callback) {
        var callbacks = this._events[eventName];
        if(!callbacks) callbacks = [];
        callbacks.push(callback);
        this._events[eventName] = callbacks;
    },
    raiseEvent: function (eventName, args) {
        if(this._isEnabled){
            var callbacks = this._events[eventName];
            
            if(callbacks){
                for (var i = 0, l = callbacks.length; i < l; i++) {
                    callbacks[i].apply(null, args);
                }    
            }
        }     
    },
    test: function () {
        this.raiseEvent('ON_TEST', [1, 2, 3]); // whatever args to pass to listeners
    },
    enable: function(){
        this._isEnabled = true;
    },
    disable: function(){
        this._isEnabled = false;
    }
};


function RemoveControl(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#ffff99';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '1px';
    controlUI.style.borderColor = '#ccc';
    controlUI.style.height = '26px';
    controlUI.style.marginTop = '5px';
    controlUI.style.marginLeft = '-6px';
    controlUI.style.paddingTop = '12px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to set the map to Home';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '10px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';
    controlText.style.marginTop = '-8px';
    controlText.innerHTML = 'Remove';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    google.maps.event.addDomListener(controlUI, 'click', function () {
        var selectedElement = map._currentSelectedElement;
        var currentManagedLayer = map.getCurrentManagedLayer();
        selectedElement.setMap(null);
        currentManagedLayer.removeComponent(selectedElement);
        map.objectBehaviorManager.removeToolTip();
        map.eventDispatcher.raiseEvent(
        GoogleMapDrawingManager.EVENTS_DRAWING_REMOVED,[selectedElement.keyCode]);
    });
}

RemoveControl._instance;
RemoveControl.getInstance = function(eventsGoogleMap){
    if(!RemoveControl._instance){
        var removeControlDiv = document.createElement('div');
        new RemoveControl(removeControlDiv, eventsGoogleMap);
        removeControlDiv.index = 1;
        RemoveControl._instance = removeControlDiv;
    }
    
    return RemoveControl._instance;
};

function UploadControl(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#ffff99';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '1px';
    controlUI.style.borderColor = '#ccc';
    controlUI.style.height = '26px';
    controlUI.style.marginTop = '5px';
    controlUI.style.marginLeft = '-7px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to upload file';
    controlDiv.appendChild(controlUI);


    var controlText2 = document.createElement('input');
    controlText2.style.fontFamily='Arial,sans-serif';
    controlText2.style.fontSize='12px';
    controlText2.style.display='inline';
    controlText2.style.width='103px';
    controlText2.name = 'image_input';
    controlText2.id = 'image_input';
    controlUI.appendChild(controlText2);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '10px';
    controlText.style.marginTop = '-8px';
    controlText.style.display='inline';
    controlText.innerHTML = 'Upload';
    controlUI.appendChild(controlText);
    
    google.maps.event.addDomListener(controlText, 'click', function () {
        var img = new Image();
        img.src = controlText2.value;
        var newComponent;
        var refreshId = setInterval(function() {
            newComponent = new overlaytiler.AffineOverlay(img);
            newComponent.setMap(map.map);
            console.log('newComponent events')
            console.log(newComponent)
            //map.eventDispatcher.raiseEvent(GoogleMapDrawingManager.IMAGE_FRAME_COMPLETE, [newComponent]);
            google.maps.event.addListener(newComponent, 'change', function (thisElement) {
                map.eventDispatcher.raiseEvent(GoogleMapDrawingManager.IMAGE_FRAME_COMPLETE, [newComponent]);
            });
            google.maps.event.addListener(newComponent, 'dbclick', function (thisElement) {
                //map.eventDispatcher.raiseEvent(GoogleMapDrawingManager.IMAGE_FRAME_COMPLETE, [newComponent]);
                alert('ariel')
            });
            clearInterval(refreshId);
        }, 1500);

    });
}

UploadControl._instance;
UploadControl.getInstance = function(eventsGoogleMap){
    if(!RemoveControl._instance){
        var uploadControlDiv = document.createElement('div');
        new UploadControl(uploadControlDiv, eventsGoogleMap);
        uploadControlDiv.index = 1;
        UploadControl._instance = uploadControlDiv;
    }
    
    return UploadControl._instance;
};
