/**
 * Handles all questions related functions
 * @author Ariel Tabag <arieltabag12@gmail.com>
 */
(function(){
    /**
     * Initialize date picker
     * @param {type} callback
     * @returns {undefined}
     */
    $.fn.bindDatePicker= function(){
        this.datetimepicker({ format: 'dd.mm.yyyy', minView: 2, pickTime: false });
    },
    /**
     * Set properties of common elements
     * @returns {undefined}
     */
    $.fn.bindElements = function(){
        this.not('.panel-activated').on('show.bs.collapse', function () {
            console.log('bindElements');
            var $this = $(this);
            $this.addClass('panel-activated');
            //init date picker
            $this.find('.startdatetimepicker').bindDatePicker();
            $this.find('.enddatetimepicker').bindDatePicker();
            //init map on map container collapse
            $this.find('div.address-collapse').bindMap(); 

        }); //end of place picker
    },
    /**
     * Initialize Map on panel collapse
     * @returns {undefined}
     */
    $.fn.bindMap = function(){
        this.on('show.bs.collapse', function () {
            var $this = $(this);
            var $target = $this.prev('div.input-group').find('input.placepicker');
            // init placepicker
            var placepicker = $target.placepicker({
              map: $this.children('div.placepicker-map').get(0)
            }).data('placepicker');

            window.setTimeout(function() {
              placepicker.resizeMap();
              placepicker.reloadMap();
              if (!$($target).prop('value')) {
                placepicker.geoLocation();
              }
            }, 0);                    
        }); //end of place picker
    },
    /**
     * Add new event form
     * @returns {undefined}
     */
    $.fn.createNewForm = function(){
        
        var methods = {
            create:function(){
                var $this = $('div.panel-event-new').clone().show().insertBefore('div.panel-event-new').removeClass('panel-event-new');
                
                $this.find('.startdatetimepicker').bindDatePicker();
                $this.find('.enddatetimepicker').bindDatePicker();
                $this.find('button.btn-collapse').attr({'href':'#collapseOne-new'});
                $this.find('div.address-collapse').attr({'id':'collapseOne-new'}).bindMap(); 
                $this.find('div.panel-collapse').attr({'id':'collapsenew'});
                $this.find('a.btn-delete').on('click', function(){ console.log($this); $this.remove() });
                $this.find('.btn-event-new').attr({'id':'POST-new'}).submitAction(function(data){ 
                    var $id = data.id;
                    $this.attr({'id':'panel-event'+$id});
                    $this.find('.btn-event-new').attr({'id':'PUT-'+$id});
                    //set new event panel headings
                    $this.find('span.span-title').html(data.title);
                    $this.find('span.span-country').html(' / '+data.country);
                    $this.find('span.span-startdate').html($this.find('input.startdate').val());
                    $this.find('span.span-enddate').html(' - ' + $this.find('input.enddate').val());

                    //button event action
                    $this.find('div.panel-collapse').attr({'id':'collapse'+$id});
                    $this.find('a.collapse-href').attr({'href':'#collapse'+$id});
                    $this.find('a.btn-hide').attr({'href':'#collapse'+$id});
                    $this.find('a.btn-edit').attr({'href':'#collapse'+$id}).show();
                    $this.find('a.btn-delete').attr({'id':'DELETE-'+$id}).submitAction(function(data){ return false;  });

                });
            }
        }
        
        this.on('click', function(){ methods.create(); });
        
    },
    
    /**
     * Submit data
     * @param {type} options
     * @returns {undefined}
     */
    $.fn.submitAction = function(callback){
        var settings = $.extend({
          api_url  : apiUrl,
          api_data : ''
        });
        
        var methods = {
            process:function($id,$action,element){
                var $url = settings.api_url;
                if($action=='DELETE'){ if(!confirm("Are you sure you want to delete this?")) return false; }
                //set api data and url
                if(jQuery.inArray($action,["DELETE","PUT"])!== -1) $url+='/'+$id;
                if(jQuery.inArray($action,["POST","PUT"])!== -1) settings.api_data = $('#collapse'+$id+' .form-control').serializeArray();

                //call rest api base on action
                jQuery.ajax({
                    type: $action,
                    url: $url,
                    data: settings.api_data,
                    dataType:"json",
                    success: function(data){
                        if($action=='DELETE' && $('#panel-event'+$id).length) $('#panel-event'+$id).remove();
                        console.log('Successful Action:'+$action);
                        element.html('Saved');
                        callback(data);
                    },
                    error: function(e){
                        console.log('Error has occurred while executing '+$action+' '+e);
                    }
                });
            }
        };
        // on click buttons
        this.on('click', function(e){ 

            var validator = true
            var $id = $(this).attr('id').split('-')[1];
            var $action = $(this).attr('id').split('-')[0];
            
            $(this).closest('div.panel-event').find('.form-control').each(function () { if($(this).val()===''){ validator=false } });
            if(validator){
                $(this).next('span.error-message').html('');
                $(this).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>  Saving...')
                //call the method baseon the action
                methods.process($id,$action,$(this));
            }else{
                $(this).next('span.error-message').html('All fields should be filled in.'); e.stopPropagation();
            }

        });
    }
}).call(this);

$(function () {
    $('div.panel-collapse').bindElements();
    $('button.btn-new-event').createNewForm();
    //triggered on submit and delete
    $('.btn-event').submitAction(function(data){ return false;  });
});