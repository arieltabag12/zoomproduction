function initMap() {
    
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
     // You are in mobile browser
        $('div.map-dynamic').hide();
        $('div.map-image').show();
    }
    var mapDiv = document.getElementById('map-canvas');
    var map = new google.maps.Map(mapDiv, {
        center: {lat: 48.210033, lng: 16.363449},
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        zoom: 3
    });

    $('#map-canvas').setMarker(map,category_json);

}

(function(){
    
    $.fn.setMarker = function(map, category){
        var settings = $.extend({
          markers  : [],
          map_image_url : ''
        });
        var methods = {
            createControl:function(category) {
                var controlDiv = document.createElement('div');
                controlDiv.index = 1;

                map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(controlDiv)
                
                // Set CSS for the control border.
                var controlUI = document.createElement('div');
                controlUI.style.backgroundColor = '#fff';
                controlUI.style.border = '2px solid #fff';
                controlUI.style.borderRadius = '3px';
                controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
                controlUI.style.cursor = 'pointer';
                controlUI.style.marginBottom = '22px';
                controlUI.style.marginRight = '10px';
                controlUI.style.textAlign = 'center';
                controlUI.title = 'Click to recenter the map';
                controlDiv.appendChild(controlUI);

                // Set CSS for the control interior.
                var controlText = document.createElement('div');
                controlText.style.color = 'rgb(25,25,25)';
                controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
                controlText.style.fontSize = '16px';
                controlText.style.lineHeight = '38px';
                controlText.style.paddingLeft = '5px';
                controlText.style.paddingRight = '5px';
                controlText.innerHTML = 'CATEGORY ' + category;
                controlUI.appendChild(controlText);

                // Setup the click event listeners: simply set the map to Chicago.
                controlUI.addEventListener('click', function() {
                    var marker_category = 'cat' + category.toLowerCase();

                    for (var i = 0; i < settings.markers.length; i++) {
                        if (settings.markers[i].category === marker_category) {
                           settings.markers[i].setVisible(true);
                         }
                         else {
                           settings.markers[i].setVisible(false);
                         }
                    }                    
                });

            },
            
            create:function(index,itemData){
                index = index+1;
                var LatLng = {lat: itemData.lat, lng: itemData.lang};
                var label = index + '. ' + itemData.title;
                var highPin = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+label+'|ff6666|FFF'; 
                
                var formattedStartDate = new Date(itemData.startdate);
                var formattedEndDate = new Date(itemData.enddate);
                var startDate = formattedStartDate.getDate() + '.' + (formattedStartDate.getMonth()+1);
                var endDate = formattedEndDate.getDate() + '.' + (formattedEndDate.getMonth()+1) + '.' + formattedEndDate.getFullYear();
                
                var contentString = '<div class="row"><div class="col-md-12">'+
                        '<div class="category-item">'+
                            '<div class="cat-top">'+
                                '<i>'+startDate+' - '+endDate+'</i>'+
                                '<span style="float:right;">CAT '+ itemData.category.replace('cat', '').toUpperCase() +'</span><br>'+
                                '<span class="category-title">'+itemData.title+'</span>'+
                                '<div class="cat-counter">'+
                                    '<span style="float:left;" class="cat-counter">'+index+'</span>'+
                                '</div><br>'+
                                '<b>'+itemData.country+'</b>'+
                            '</div>'+
                            '<div class="cat-bottom">'+
                                '<span class="text">'+itemData.description+'</span><br>'+
                            '</div>'+
                        '</div>'+
                    '</div></div>';

                //static image options
                settings.map_image_url += '&amp;markers='+itemData.lat+','+itemData.lang;

                var infowindow = new google.maps.InfoWindow({ content: contentString,maxWidth: 342 });

                var marker = new google.maps.Marker({
                  position: LatLng,
                  map: map,
                  title: itemData.title,
                  icon: highPin
                });
                
                marker.category = itemData.category;
                //add click on marker
                marker.addListener('click', function() { infowindow.open(map, marker); map.setCenter(marker.getPosition()); });
                
                $('#show-map-'+itemData.id).on('click', function(){ 
                    infowindow.open(map, marker); 
                    $('html,body').animate({scrollTop: $('#map-canvas').offset().top},'slow');
                });
                
                settings.markers.push(marker);
                
                
            }
        }
        jQuery.each(category, function(index, itemData) {
            methods.create(index,itemData);
        });
        
        methods.createControl('A');
        methods.createControl('B');
 
       
        //static image map
        $('.btn-activate-map').on('click', function(){
            $('div.map-dynamic').show();
            $('div.map-image').hide();
        })
        var cval = "https://maps.googleapis.com/maps/api/staticmap?zoom=2&amp;size=570x250&amp;scale=2&amp;sensor=false&amp;maptype=satellite"+settings.map_image_url;
        $('<img style="width:100%" src="'+ cval +'">').insertAfter('button.btn-activate-map');
        
    }
}).call(this);

